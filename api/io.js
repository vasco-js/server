module.exports = function(vasco) {
  let io = require("socket.io")(vasco.server.server, {
    pingTimeout: 60000,
    upgradeTimeout: 60000 // default value is 10000ms, try changing it to 20k or more
  });

  let algorithms = vasco.algorithms;
  io.on("connection", function(socket) {
    // algorithms endpoint
    socket.on("run", function({ name, props }) {
      vasco.algorithms.run(name, props, socket);
    });
  });
};
