let express = require("express");
let http = require("http");

let configureApi = require("./api");

class VascoServer {
  constructor(vasco) {
    this.vasco = vasco;
    this.config = vasco.config;

    this.app = express();
    this.server = http.createServer(this.app);
  }
  init() {
    configureApi(this.vasco);
    // AFTER API
    if (!this.config.get("client.disable"))
      this.app.use(this.vasco.client.render);
  }
  start() {
    let port = this.config.get("server.port");
    let url = this.url;
    let logger = this.vasco.logger;

    this.server.listen(port, () => {
      logger.info(`vasco server available at ${url}`);
    });
  }
  stop() {
    this.server.close();
  }
  get url() {
    let { ssl, host, port } = this.config.get("server");
    return `${ssl.enabled ? "https" : "http"}://${host}:${port}`;
  }
}

module.exports = VascoServer;
